package com.nasoft.sse.entities;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by Atanas Alexandrov on 13.09.17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StockTransaction {
    String user;
    Stock stock;
    Date when;
}