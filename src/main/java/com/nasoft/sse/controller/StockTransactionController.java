package com.nasoft.sse.controller;

import org.springframework.http.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nasoft.sse.entities.StockTransaction;
import com.nasoft.sse.services.StockTransactionService;

import reactor.core.publisher.Flux;

/**
 * Created by Atanas Alexandrov on 13.09.17.
 */
@RestController
@RequestMapping("/stock/transaction")
public class StockTransactionController {

    @Autowired
    StockTransactionService stockTransactionService;

    @GetMapping(produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<StockTransaction> stockTransactionEvents(){
        return stockTransactionService.getStockTransactions();
    }
}
